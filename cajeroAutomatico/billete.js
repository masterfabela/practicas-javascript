class Billete {
  constructor(valor, cantidad, urlImagen) {
    this.valor = valor;
    this.cantidad = cantidad;
    this.imagen = new Image();
    this.imagen.src = urlImagen;
  }

  mostrar() {
    var billetes = document.getElementById("billetes");
    billetes.appendChild(this. )
    billetes.innerHTML += this.imagen;
    var resultado = document.getElementById("resultado");
    resultado.innerHTML = "<p>" + this.cantidad + "</p>";
  }
}
